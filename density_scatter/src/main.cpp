#include "xtensor/xtensor.hpp"
#include "xtensor/xeval.hpp"
#define FORCE_IMPORT_ARRAY
#include "xtensor-python/pytensor.hpp"

using vector_type = xt::pytensor<double, 1>;
using matrix_type = xt::pytensor<double, 2>;
using size_type = vector_type::size_type;

namespace py = pybind11;

double get_surface(const vector_type& xmesh, const vector_type& ymesh)
{
    size_type xmax = xmesh.size() - 1;
    size_type ymax = ymesh.size() - 1;
    return (xmesh(xmax) - xmesh(0)) * (ymesh(ymax) - ymesh(0));
}

double normalization_constant(const matrix_type& values, double surface)
{
    return (xt::sum(values) * surface)();
}

void fill_density(const vector_type& xmesh,
                  const vector_type& ymesh,
                  const matrix_type& points,
                  double sigma,
                  matrix_type& density)
{
    size_type xsize = xmesh.size();
    size_type ysize = ymesh.size();
    density.reshape({static_cast<npy_intp>(xsize), static_cast<npy_intp>(ysize)});
    vector_type tmp = {0, 0};
    double factor = 0.5 / (sigma * sigma);

    for(size_type i = 0; i < xsize; ++i)
    {
        for(size_type j = 0; j < ysize; ++j)
        {
            tmp(0) = xmesh(i);
            tmp(1) = ymesh(j);
            
            vector_type dist = xt::sum(xt::pow(tmp - points, 2), {1});
            density(i, j) = (xt::sum(xt::exp(-dist * factor)))();
        }
    }
}

matrix_type density(const vector_type& xmesh,
                    const vector_type& ymesh,
                    const matrix_type& points,
                    double sigma)
{
    matrix_type res;
    fill_density(xmesh, ymesh, points, sigma, res);
    double norm_factor = normalization_constant(res, get_surface(xmesh, ymesh));
    return res / norm_factor;
}

PYBIND11_PLUGIN(density_scatter)
{
    xt::import_numpy();

    py::module m("density_scatter", "Module that computes the density of a scatter plot");
    m.def("density", &density, py::arg("xmesh"), py::arg("ymesh"), py::arg("points"), py::arg("sigma"));

    return m.ptr();
}

