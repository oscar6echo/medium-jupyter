#!/bin/bash

set -e

REPOSRC=https://gitlab.com/oscar6echo/medium-jupyter.git
LOCALREPO=medium-jupyter

# We do it this way so that we can abstract if from just git later on
LOCALREPO_VC_DIR=$LOCALREPO/.git


useradd ${JPY_USER} || true
chown ${JPY_USER} /work
su ${JPY_USER}
cd /work

# Clone if the repo does not exist, pull otherwise
if [ ! -d $LOCALREPO_VC_DIR ]
then
      git clone $REPOSRC $LOCALREPO
    else
          cd $LOCALREPO
              git pull $REPOSRC
            fi

exec "$@"
